using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilitarCamaraSec : MonoBehaviour
{
    public GameObject CamaraPrincipal;
    public GameObject CamaraSec1;
    public GameObject CamaraSec2;
    public GameObject CamaraSec3;
    public GameObject BotonesMenuPrincipal;
    public GameObject VerDeCerca_PB_CamaraSec;
    public LNAmbientes lnambientes;
    public GameObject Interfaz_Plataforma;

    public GameObject desi;

    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "CamaraSec1")
                {
                    ActivarMenu_CamaraSec();
                    CamaraSec1.SetActive(true);
                    VerDeCerca_PB_CamaraSec.SetActive(true);
                }
                if (hit.transform.tag == "CamaraSec2")
                {
                    ActivarMenu_CamaraSec();
                    CamaraSec2.SetActive(true);
                    VerDeCerca_PB_CamaraSec.SetActive(true);
                }
                if (hit.transform.tag == "CamaraSec3")
                {
                    ActivarMenu_CamaraSec();
                    CamaraSec3.SetActive(true);
                    VerDeCerca_PB_CamaraSec.SetActive(true);
                }
                if (hit.transform.tag == "MP_Plataforma")
                {
                    desi.SetActive(false);
                    ActivarMenu_InterfazPlataforma();
                    StartCoroutine(ActivadorPlataforma());
                }
            }
        }
    }

    public IEnumerator ActivadorPlataforma()
    {
        lnambientes.ActicarCourutineAmbientes_MP_Plataforma();
        yield return new WaitForSeconds(0.5f);
        Interfaz_Plataforma.SetActive(true);
    }

    public void ActivarMenu_CamaraSec()
    {
        CamaraPrincipal.SetActive(false);
        CamaraSec1.SetActive(false);
        CamaraSec2.SetActive(false);
        CamaraSec3.SetActive(false);
        BotonesMenuPrincipal.SetActive(false);
        VerDeCerca_PB_CamaraSec.SetActive(false);
        desi.SetActive(false);
    }

    public void ActivarMenu_InterfazPlataforma()
    {
        BotonesMenuPrincipal.SetActive(false);
    }

    public void Salir_Camara_Sec()
    {
        ActivarMenu_CamaraSec();
        CamaraPrincipal.SetActive(true);
        BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }

    public void DesactivarMenuInformacion()
    {
        Interfaz_Plataforma.SetActive(false);
        BotonesMenuPrincipal.SetActive(true);
        desi.SetActive(true);
    }
}
