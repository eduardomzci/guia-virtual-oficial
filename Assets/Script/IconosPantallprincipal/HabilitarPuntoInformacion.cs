using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilitarPuntoInformacion : MonoBehaviour
{
    public GameObject BancoGanadero;
    //buscar Script LNAmbientes
    public LNAmbientes lnambientes;

    public GameObject desi;

    //public GameObject Postgrado;
    //public GameObject Comedor;
    //public GameObject Gastro;
    //public GameObject Tecnologia;
    //public GameObject CS_A;
    //public GameObject CS_C;

    public GameObject MenuBotonesPantallaPrincipal;
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "BancoGanadero")
                {
                    ActivarmenuInformacion();
                    StartCoroutine(ActivadorBancoEconomico());
                }
                if (hit.transform.tag == "Modulo8")
                {
                    ActivarmenuInformacion();
                    StartCoroutine(ActivadorModulo8());
                }
                if (hit.transform.tag == "Modulo9")
                {
                    ActivarmenuInformacion();
                    StartCoroutine(ActivadorModulo9());
                }
            }
        }
    }

    public IEnumerator ActivadorBancoEconomico()
    {
        lnambientes.ActicarCourutineAmbientes_BancoEconomico();
        yield return new WaitForSeconds(0.5f);
        BancoGanadero.SetActive(true);
    }
    public IEnumerator ActivadorModulo8()
    {
        lnambientes.ActicarCourutineAmbientes_Modulo8();
        yield return new WaitForSeconds(0.5f);
        BancoGanadero.SetActive(true);
    }
    public IEnumerator ActivadorModulo9()
    {
        lnambientes.ActicarCourutineAmbientes_Modulo9();
        yield return new WaitForSeconds(0.5f);
        BancoGanadero.SetActive(true);
    }

    public void ActivarmenuInformacion()
    {
        MenuBotonesPantallaPrincipal.SetActive(false);
        desi.SetActive(false);
        //Postgrado.SetActive(false);
        //Comedor.SetActive(false);
        //Gastro.SetActive(false);
        //Tecnologia.SetActive(false);
        //CS_A.SetActive(false);
        //CS_C.SetActive(false);
    }

    public void DesactivarMenuInformacion()
    {
        //Vuforia_Facultad_Destino_Script vuf = GameObjectScriptActivarFacultad.GetComponent<Vuforia_Facultad_Destino_Script>();
        //StartCoroutine(vuf.tempo());
        BancoGanadero.SetActive(false);
        MenuBotonesPantallaPrincipal.SetActive(true);
        desi.SetActive(true);
    }
}
