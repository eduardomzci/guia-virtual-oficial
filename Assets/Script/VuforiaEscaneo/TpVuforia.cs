using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TpVuforia : MonoBehaviour
{
    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Facultad";
    public int ubicacion_Destino;
    //

    //--- Guardar datos de Destino -- Postgrado
    public string F_Destino_Postgrado = "Postgrado";
    public int ubicacion_Destino_Postgrado;
    //

    public Animator transition;
    public float transitionTime = 1f;

    public void cambiarEscena_MapaPrincipal_Postgrado()
    {
        //--- Guardar datos de Destino -- Postgrado
        ubicacion_Destino = 1;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void cambiarEscena_MapaPrincipal_Comedor()
    {
        //--- Guardar datos de Destino -- Comedor
        ubicacion_Destino = 2;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void cambiarEscena_MapaPrincipal_Gastronomia()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino = 3;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
        //SceneManager.LoadScene("EliminarEscenaPrueba", LoadSceneMode.Single);
    }

    public void cambiarEscena_MapaPrincipal_Tecnologia()
    {
        //--- Guardar datos de Destino -- Tecnologia
        ubicacion_Destino = 4;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void cambiarEscena_MapaPrincipal_CienciasdelaSalud_ModuloA()
    {
        //--- Guardar datos de Destino -- CienciasdelaSaludModuloA
        ubicacion_Destino = 5;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void cambiarEscena_MapaPrincipal_CienciasdelaSalud_ModuloC()
    {
        //--- Guardar datos de Destino -- CienciasdelaSaludModuloC
        ubicacion_Destino = 6;
        PlayerPrefs.SetInt(F_Destino, ubicacion_Destino);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_MapaPrincipal();
    }

    public void cambiarEscena_Postgrado_PB_LD()
    {
        //--- Guardar datos de Destino -- CienciasdelaSaludModuloC
        ubicacion_Destino_Postgrado = 101;
        PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
        //
        //SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
        LoadNextLevel_Postgrado();
    }
    public void cambiarEscena_Postgrado_PB_MD()
    {
        //--- Guardar datos de Destino -- CienciasdelaSaludModuloC
        ubicacion_Destino_Postgrado = 102;
        PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
        //
        LoadNextLevel_Postgrado();
    }
    public void cambiarEscena_Postgrado_PB_IZ()
    {
        //--- Guardar datos de Destino -- CienciasdelaSaludModuloC
        ubicacion_Destino_Postgrado = 103;
        PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
        //
        LoadNextLevel_Postgrado();
    }

    public void LoadNextLevel_MapaPrincipal()
    {
        StartCoroutine(levelLoader_MapaPrincipal());
    }

    public void LoadNextLevel_Postgrado()
    {
        StartCoroutine(levelLoader_Postgrado());
    }

    public IEnumerator levelLoader_MapaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("MapaPrincipal", LoadSceneMode.Single);
    }

    public IEnumerator levelLoader_Postgrado()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
    }
}
