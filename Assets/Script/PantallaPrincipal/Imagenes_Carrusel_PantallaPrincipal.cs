using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Imagenes_Carrusel_PantallaPrincipal : MonoBehaviour
{

    public Animator image;
    public Animator image1;
    public Animator image2;
    public Animator image3;

    public GameObject GO_image;
    public GameObject GO_image1;
    public GameObject GO_image2;
    public GameObject GO_image3;


    public void desactivarImages()
    {
        GO_image.SetActive(false);
        GO_image1.SetActive(false);
        GO_image2.SetActive(false);
        GO_image3.SetActive(false);
    }
    void Start()
    {
        StartCoroutine(AnimacionCarrusel());
    }

    //public void Desaparecer_Image()
    //{
    //    image.Play("Image_Desaparecer");
    //}

    public IEnumerator AnimacionCarrusel()
    {
        for (int i = 0; i < 100; i++)
        {
            GO_image.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image3.SetActive(false);
            image.SetTrigger("End");

            GO_image1.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image.SetActive(false);
            image1.SetTrigger("End");

            GO_image2.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image1.SetActive(false);
            image2.SetTrigger("End");

            GO_image3.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image2.SetActive(false);
            image3.SetTrigger("End");

            //desactivarImages();
        }
    }
}
