using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EInterfazSimple : MonoBehaviour
{
    public int interfazID { get; set; }
    public int uiID { get; set; }
    public string nombreTextoReferencia { get; set; }
    public string textoEditarInterfaz { get; set; }
}
