using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EU_EAmbienteSimple : MonoBehaviour
{
    public int ambienteID { get; set; }
    public int sectorID { get; set; }
    public string identificadorAmbiente { get; set; }
    public string nombreAppAmbiente { get; set; }
    public string descripcionAppAmbiente { get; set; }
    public byte[] imagen1 { get; set; }
    public byte[] imagen2 { get; set; }

}
