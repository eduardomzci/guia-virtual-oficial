using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class LNInterfaz_MP : MonoBehaviour
{
    #region Propiedades
    //Menu MP_F_Postgrado
    public Text txt_TituloPrincipal6;
    public Text PB_Texto1;
    public Text btn_Plataforma1;
    public Text btn_BancoEconomico1;
    public Text btn_SaladeArte1;
    public Text txt_Aviso;

    //Menu MP_LetrerosPuntosDesignados
    public TextMeshPro txt_Letrero_Plataforma;
    public TextMeshPro txt_Letrero_SaladeArte;
    public TextMeshPro txt_Letrero_BancoEconomico;
    public TextMeshPro txt_Letrero_Entrada;

    #endregion

    #region MetodosPublicos

    void Start()
    {
        ActicarCourutine_Menu_MP_LetrerosPuntosDesignados();
    }


    public void ActicarCourutine_Menu_MP_F_Postgrado()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_F_Postgrado());
    }

    public void ActicarCourutine_Menu_MP_LetrerosPuntosDesignados()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados());
    }

    public IEnumerator Obtener_InterfazID_Menu_MP_F_Postgrado()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/24";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal6.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/25";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        PB_Texto1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/26";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Plataforma1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/27";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_BancoEconomico1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi4 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/28";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi4))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_SaladeArte1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi5 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/30";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi5))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public IEnumerator Obtener_InterfazID_Menu_MP_LetrerosPuntosDesignados()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/37";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Letrero_Plataforma.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/38";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Letrero_SaladeArte.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/39";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Letrero_BancoEconomico.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/40";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Letrero_Entrada.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    #endregion
}
