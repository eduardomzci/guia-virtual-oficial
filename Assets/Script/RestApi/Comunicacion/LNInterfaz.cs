using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using TMPro;

public class LNInterfaz : MonoBehaviour
{
    #region Propiedades
    //Menu_MMP_Elementos
    public Text txt_titulo;
    public Text btn_Tecnologia;
    public Text btn_CienciasdelaSalud;
    public Text btn_Comedor;
    public Text btn_Gastronomia;
    public Text btn_Postgrado;

    //Menu_MMP_F_Postgrado
    public Text txt_TituloPrincipal;
    public Text PB_Texto;
    public Text btn_Plataforma;
    public Text btn_BancoEconomico;
    public Text btn_SaladeArte;
    public Text txt_Aviso4;

    //MMP_F_Gastronomia
    public Text txt_TituloPrincipal1;
    public Text txt_Aviso;

    //Menu_MMP_F_Tecnologia
    public Text txt_TituloPrincipal2;
    public Text txt_Aviso1;

    //Menu_MMP_ModulosCienciasdelaSalud
    public Text txt_TituloPrincipal3;
    public Text btn_ModuloA;
    public Text btn_ModuloC;

    //Menu_MMP_F_CienciasDeLaSalud_ModuloA
    public Text txt_TituloPrincipal4;
    public Text txt_Aviso2;

    //Menu_MMP_F_CienciasDeLaSalud_ModuloC
    public Text txt_TituloPrincipal5;
    public Text txt_Aviso3;

    //Menu MMP_Aviso_UstedSeEncuentraennelLugarSelcciona
    public Text txt_AvisoTexto;

    //Menu MMP_LetrerosFacultades
    public TextMeshPro txt_Modulo1_1;
    public TextMeshPro txt_Modulo1_2;
    public TextMeshPro txt_Modulo2_1;
    public TextMeshPro txt_Modulo2_2;
    public TextMeshPro txt_Modulo3A_1;
    public TextMeshPro txt_Modulo3A_2;
    public TextMeshPro txt_Modulo3C_1;
    public TextMeshPro txt_Modulo3C_2;
    public TextMeshPro txt_Modulo4_1;
    public TextMeshPro txt_Modulo4_2;
    public TextMeshPro txt_Modulo5_1;
    public TextMeshPro txt_Modulo5_2;

    ////Menu MP_F_Postgrado
    //public Text txt_TituloPrincipal6;
    //public Text PB_Texto1;
    //public Text btn_Plataforma1;
    //public Text btn_BancoEconomico1;
    //public Text btn_SaladeArte1;


    #endregion

    #region MetodosPublicos


    void Start()
    {
        ActicarCourutine_Menu_MMP_LetrerosFacultades();
    }

    public void ActicarCourutine_Menu_MMP_Elementos()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_Elementos());
    }
    public void ActicarCourutine_Menu_MMP_F_Postgrado()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Postgrado());
    }
    public void ActicarCourutine_Menu_MMP_F_Gastronomia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Gastronomia());
    }
    public void ActicarCourutine_Menu_Menu_MMP_F_Tecnologia()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_Tecnologia());
    }
    public void ActicarCourutine_Menu_MMP_ModulosCienciasdelaSalud()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_ModulosCienciasdelaSalud());
    }
    public void ActicarCourutine_Menu_MMP_F_CienciasDeLaSalud_ModuloA()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloA());
    }
    public void ActicarCourutine_Menu_MMP_F_CienciasDeLaSalud_ModuloC()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloC());
    }
    public void ActicarCourutine_Menu_MMP_Aviso_UstedSeEncuentraennelLugarSelcciona()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_Aviso_UstedSeEncuentraennelLugarSelcciona());
    }

    public void ActicarCourutine_Menu_MMP_LetrerosFacultades()
    {
        StartCoroutine(Obtener_InterfazID_Menu_MMP_LetrerosFacultades());
    }
    //public void ActicarCourutine_Menu_MP_F_Postgrado()
    //{
    //    StartCoroutine(Obtener_InterfazID_Menu_MP_F_Postgrado());
    //}


    public IEnumerator Obtener_InterfazID_Menu_MMP_Elementos()
    {

        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/1";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_titulo.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/2";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Tecnologia.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/6";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_CienciasdelaSalud.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/7";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Comedor.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi4 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/8";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi4))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Gastronomia.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi5 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/9";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi5))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Postgrado.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Postgrado()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/3";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/4";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        PB_Texto.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/10";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_Plataforma.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/11";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_BancoEconomico.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        string urlApi4 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/12";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi4))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_SaladeArte.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi5 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/29";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi5))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso4.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Gastronomia()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/5";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/13";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_Tecnologia()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/14";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/15";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_ModulosCienciasdelaSalud()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/16";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal3.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/17";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_ModuloA.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/18";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        btn_ModuloC.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloA()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/19";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal4.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/20";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_F_CienciasDeLaSalud_ModuloC()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/21";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_TituloPrincipal5.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/22";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Aviso3.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_Aviso_UstedSeEncuentraennelLugarSelcciona()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/23";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_AvisoTexto.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }

    public IEnumerator Obtener_InterfazID_Menu_MMP_LetrerosFacultades()
    {
        string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/31";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo1_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo1_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/32";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo2_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo2_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/33";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo3A_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo3A_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/34";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo3C_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo3C_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi4 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/35";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi4))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo4_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo4_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        string urlApi5 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/36";
        using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi5))
        {
            yield return cliente.SendWebRequest();
            try
            {
                switch (cliente.result)
                {
                    case UnityWebRequest.Result.Success:
                        string ResultadoJson = cliente.downloadHandler.text;
                        EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
                        //Texto Entidad
                        txt_Modulo5_1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        txt_Modulo5_2.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

    //public IEnumerator Obtener_InterfazID_Menu_MP_F_Postgrado()
    //{
    //    string urlApi = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/24";
    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    txt_TituloPrincipal6.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }

    //    string urlApi1 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/25";
    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi1))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    PB_Texto1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }

    //    string urlApi2 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/26";
    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi2))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    btn_Plataforma1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }

    //    string urlApi3 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/27";
    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi3))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    btn_BancoEconomico1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }

    //    string urlApi4 = "https://localhost:44392/api/Guiavirtual/BuscadorInterfaz/28";
    //    using (UnityWebRequest cliente = UnityWebRequest.Get(urlApi4))
    //    {
    //        yield return cliente.SendWebRequest();
    //        try
    //        {
    //            switch (cliente.result)
    //            {
    //                case UnityWebRequest.Result.Success:
    //                    string ResultadoJson = cliente.downloadHandler.text;
    //                    EU_EInterfazSimple eu_EInterfazSimple = JsonConvert.DeserializeObject<EU_EInterfazSimple>(ResultadoJson);
    //                    //Texto Entidad
    //                    btn_SaladeArte1.text = "" + eu_EInterfazSimple.textoEditarInterfaz;
    //                    break;
    //            }
    //        }
    //        catch (Exception ex)
    //        {

    //            throw ex;
    //        }
    //    }
    //}
    #endregion
}
