using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapaPrincipal_TodasLasFactultades : MonoBehaviour
{
    //MenuBotonesMenuPrincipal
    public GameObject GO_MenuBotonesMenuPrincipal;
    //
    //public GameObject GO_SalirMenuPantallaPrincipal;
    //public GameObject GO_Menu;
    //public GameObject GO_CambiarCamara;

    public GameObject GO_CamaraRotacionMain;
    public float xCambiarCamara;
    public GameObject GO_MenuElementos;
    public GameObject GO_FGastronomia;
    public GameObject GO_FGastronomia_SgtEscena;
    //----
    public GameObject GO_FPostgrado;
    public GameObject GO_FPostgrado_SgtEscena;
    public GameObject GO_FTecnologia;
    public GameObject GO_FTecnologia_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloA;
    public GameObject GO_FCienciasDeLaSalud_ModuloC;
    public GameObject GO_FCienciasDeLaSalud_ModuloA_SgtEscena;
    public GameObject GO_FCienciasDeLaSalud_ModuloC_SgtEscena;
    public GameObject GO_ModuloCienciasdelaSalud;
    //----
    public GameObject GO_DeslizamientoZoom;

    //---------------------------------------------------------------------------------------------------
    //--Postgrado
    public GameObject Flechas_Postgrado_Comedor;
    public GameObject Flechas_Postgrado_Tecnologia;
    public GameObject Flechas_Postgrado_Gastronomia;
    public GameObject Flechas_Postgrado_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Postgrado_CienciasdelaSalud_ModuloC;
    //--
    //--Comedor
    public GameObject Flechas_Comedor_Postgrado;
    public GameObject Flechas_Comedor_Tecnologia;
    public GameObject Flechas_Comedor_Gastronomia;
    public GameObject Flechas_Comedor_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Comedor_CienciasdelaSalud_ModuloC;
    //--
    //--Gastroniomia
    public GameObject Flechas_Gastronomia_Postgrado;
    public GameObject Flechas_Gastronomia_Tecnologia;
    public GameObject Flechas_Gastronomia_Comedor;
    public GameObject Flechas_Gastronomia_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Gastronomia_CienciasdelaSalud_ModuloC;
    //--
    //--Tecnologia
    public GameObject Flechas_Tecnologia_Postgrado;
    public GameObject Flechas_Tecnologia_Gastronomia;
    public GameObject Flechas_Tecnologia_Comedor;
    public GameObject Flechas_Tecnologia_CienciasdelaSalud_ModuloA;
    public GameObject Flechas_Tecnologia_CienciasdelaSalud_ModuloC;
    //--
    //--CS_A
    public GameObject Flechas_CS_ModuloA_Postgrado;
    public GameObject Flechas_CS_ModuloA_Gastronomia;
    public GameObject Flechas_CS_ModuloA_Comedor;
    public GameObject Flechas_CS_ModuloA_Tecnologia;
    public GameObject Flechas_CS_ModuloA_CienciasdelaSalud_ModuloC;
    //--
    //--CS_C
    public GameObject Flechas_CS_ModuloC_Postgrado;
    public GameObject Flechas_CS_ModuloC_Gastronomia;
    public GameObject Flechas_CS_ModuloC_Comedor;
    public GameObject Flechas_CS_ModuloC_Tecnologia;
    public GameObject Flechas_CS_ModuloC_CienciasdelaSalud_ModuloA;
    //--
    //----------------------------------------------------------------------------------------------------

    //--
    public GameObject Aviso_UstedSeEncuentraennelLugarSelccionado;
    //--

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Facultad";
    public int ubicacion_Destino;
    //

    //--- Guardar datos de Destino -- Postgrado
    public string F_Destino_Postgrado = "Postgrado";
    public int ubicacion_Destino_Postgrado;
    //

    //--- Guardar datos de Destino -- Gastronomia
    public string F_Destino_Gastronomia = "Gastronomia";
    public int ubicacion_Destino_Gastronomia;
    //

    //--AnimacionCircle
    public Animator transition;
    public float transitionTime = 1f;
    //

    //Banderas
    public bool Bandera_Postgrado=false;
    public bool Bandera_Comedor = false;
    public bool Bandera_Gastronomia = false;
    public bool Bandera_Tecnologia = false;
    public bool Bandera_CS_A = false;
    public bool Bandera_CS_C = false;

    //Recuperar Datos de RestApi_UstedSeEncuentraAqui
    public LNInterfaz lninterfaz;

    public void DesactivadorDeBanderas_Facultades()
    {
        Bandera_Postgrado = false;
        Bandera_Comedor = false;
        Bandera_Gastronomia = false;
        Bandera_Tecnologia = false;
        Bandera_CS_A = false;
        Bandera_CS_C = false;
    }

    public IEnumerator PB_HabilitarCaminos_Facultad()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            Bandera_Postgrado = true;
            yield return null;
        }
        else if (ubicacion_Destino == 2)
        {
            Bandera_Comedor = true;
            yield return null;
        }
        else if (ubicacion_Destino == 3)
        {
            Bandera_Gastronomia = true;
            yield return null;
        }
        else if (ubicacion_Destino == 4)
        {
            Bandera_Tecnologia = true;
            yield return null;
        }
        else if (ubicacion_Destino == 5)
        {
            Bandera_CS_A = true;
            yield return null;
        }
        else if (ubicacion_Destino == 6)
        {
            Bandera_CS_C = true;
            yield return null;
        }
    }

    public IEnumerator Temporizador3Segs_AvisoLugarSeleccionado()
    {
        //Inclusion Recuperar dato Rest Api
        lninterfaz.ActicarCourutine_Menu_MMP_Aviso_UstedSeEncuentraennelLugarSelcciona();
        //
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(true);
        yield return new WaitForSeconds(3);
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
    }

    public void habilitarBoton_Postgrado()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        else if (Bandera_Comedor == true)
        {
            Flechas_Comedor_Postgrado.SetActive(true);
        }
        else if (Bandera_Gastronomia == true)
        {
            Flechas_Gastronomia_Postgrado.SetActive(true);
        }
        else if (Bandera_Tecnologia == true)
        {
            Flechas_Tecnologia_Postgrado.SetActive(true);
        }
        else if (Bandera_CS_A == true)
        {
            Flechas_CS_ModuloA_Postgrado.SetActive(true);
        }
        else if (Bandera_CS_C == true)
        {
            Flechas_CS_ModuloC_Postgrado.SetActive(true);
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void habilitarBoton_Comedor()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            Flechas_Postgrado_Comedor.SetActive(true);
        }
        else if (Bandera_Comedor == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        else if (Bandera_Gastronomia == true)
        {
            Flechas_Gastronomia_Comedor.SetActive(true);
        }
        else if (Bandera_Tecnologia == true)
        {
            Flechas_Tecnologia_Comedor.SetActive(true);
        }
        else if (Bandera_CS_A == true)
        {
            Flechas_CS_ModuloA_Comedor.SetActive(true);
        }
        else if (Bandera_CS_C == true)
        {
            Flechas_CS_ModuloC_Comedor.SetActive(true);
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void habilitarBoton_Gastronomia()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            Flechas_Postgrado_Gastronomia.SetActive(true);
        }
        else if (Bandera_Comedor == true)
        {
            Flechas_Comedor_Gastronomia.SetActive(true);
        }
        else if (Bandera_Gastronomia == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        else if (Bandera_Tecnologia == true)
        {
            Flechas_Tecnologia_Gastronomia.SetActive(true);
        }
        else if (Bandera_CS_A == true)
        {
            Flechas_CS_ModuloA_Gastronomia.SetActive(true);
        }
        else if (Bandera_CS_C == true)
        {
            Flechas_CS_ModuloC_Gastronomia.SetActive(true);
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void habilitarBoton_Tecnologia()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            Flechas_Postgrado_Tecnologia.SetActive(true);
        }
        else if (Bandera_Comedor == true)
        {
            Flechas_Comedor_Tecnologia.SetActive(true);
        }
        else if (Bandera_Gastronomia == true)
        {
            Flechas_Gastronomia_Tecnologia.SetActive(true);
        }
        else if (Bandera_Tecnologia == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        else if (Bandera_CS_A == true)
        {
            Flechas_CS_ModuloA_Tecnologia.SetActive(true);
        }
        else if (Bandera_CS_C == true)
        {
            Flechas_CS_ModuloC_Tecnologia.SetActive(true);
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void habilitarBoton_CS_A()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            Flechas_Postgrado_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        else if (Bandera_Comedor == true)
        {
            Flechas_Comedor_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        else if (Bandera_Gastronomia == true)
        {
            Flechas_Gastronomia_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        else if (Bandera_Tecnologia == true)
        {
            Flechas_Tecnologia_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        else if (Bandera_CS_A == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        else if (Bandera_CS_C == true)
        {
            Flechas_CS_ModuloC_CienciasdelaSalud_ModuloA.SetActive(true);
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void habilitarBoton_CS_C()
    {
        DesactivarCaminos_TodosLosCaminos();
        StartCoroutine(PB_HabilitarCaminos_Facultad());
        if (Bandera_Postgrado == true)
        {
            Flechas_Postgrado_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        else if (Bandera_Comedor == true)
        {
            Flechas_Comedor_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        else if (Bandera_Gastronomia == true)
        {
            Flechas_Gastronomia_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        else if (Bandera_Tecnologia == true)
        {
            Flechas_Tecnologia_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        else if (Bandera_CS_A == true)
        {
            Flechas_CS_ModuloA_CienciasdelaSalud_ModuloC.SetActive(true);
        }
        else if (Bandera_CS_C == true)
        {
            StartCoroutine(Temporizador3Segs_AvisoLugarSeleccionado());
        }
        DesactivadorDeBanderas_Facultades();
    }

    public void SalirMenuIconos()
    {
        DesactivarElementosDePantalla();
        GO_MenuBotonesMenuPrincipal.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }

    public void DesactivarElementosDePantalla()
    {
        GO_MenuBotonesMenuPrincipal.SetActive(false);
        GO_MenuElementos.SetActive(false);
        GO_FGastronomia.SetActive(false);
        GO_FGastronomia_SgtEscena.SetActive(false);
        GO_FPostgrado.SetActive(false);
        GO_FPostgrado_SgtEscena.SetActive(false);
        GO_FTecnologia.SetActive(false);
        GO_FTecnologia_SgtEscena.SetActive(false);
        GO_ModuloCienciasdelaSalud.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloA_SgtEscena.SetActive(false);
        GO_FCienciasDeLaSalud_ModuloC_SgtEscena.SetActive(false);
    }

    public void DesactivarCaminos_TodosLosCaminos()
    {
        //AvisoSeEncuentraEnElLugarSeleccionado
        Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //Postgrado
        Flechas_Postgrado_Comedor.SetActive(false);
        Flechas_Postgrado_Tecnologia.SetActive(false);
        Flechas_Postgrado_Gastronomia.SetActive(false);
        Flechas_Postgrado_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Postgrado_CienciasdelaSalud_ModuloC.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //Comedor
        Flechas_Comedor_Postgrado.SetActive(false);
        Flechas_Comedor_Tecnologia.SetActive(false);
        Flechas_Comedor_Gastronomia.SetActive(false);
        Flechas_Comedor_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Comedor_CienciasdelaSalud_ModuloC.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //Gastronomia
        Flechas_Gastronomia_Postgrado.SetActive(false);
        Flechas_Gastronomia_Tecnologia.SetActive(false);
        Flechas_Gastronomia_Comedor.SetActive(false);
        Flechas_Gastronomia_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Gastronomia_CienciasdelaSalud_ModuloC.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //Tecnologia
        Flechas_Tecnologia_Postgrado.SetActive(false);
        Flechas_Tecnologia_Gastronomia.SetActive(false);
        Flechas_Tecnologia_Comedor.SetActive(false);
        Flechas_Tecnologia_CienciasdelaSalud_ModuloA.SetActive(false);
        Flechas_Tecnologia_CienciasdelaSalud_ModuloC.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //CS_A
        Flechas_CS_ModuloA_Postgrado.SetActive(false);
        Flechas_CS_ModuloA_Gastronomia.SetActive(false);
        Flechas_CS_ModuloA_Comedor.SetActive(false);
        Flechas_CS_ModuloA_Tecnologia.SetActive(false);
        Flechas_CS_ModuloA_CienciasdelaSalud_ModuloC.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
        //CS_C
        Flechas_CS_ModuloC_Postgrado.SetActive(false);
        Flechas_CS_ModuloC_Gastronomia.SetActive(false);
        Flechas_CS_ModuloC_Comedor.SetActive(false);
        Flechas_CS_ModuloC_Tecnologia.SetActive(false);
        Flechas_CS_ModuloC_CienciasdelaSalud_ModuloA.SetActive(false);
        //Aviso_UstedSeEncuentraennelLugarSelccionado.SetActive(false);
        //
    }

    public void ActivarRecorrido_Postgrado_Comedor()
    {
        DesactivarCaminos_TodosLosCaminos();
        Flechas_Postgrado_Comedor.SetActive(true);
    }

    public void Menu()
    {
        StartCoroutine(II_Menu());
    }

    //Prueba de Ienumerator para darle delay al tiempo de respuesta de Rest Api
    public IEnumerator II_Menu()
    {
        yield return new WaitForSeconds(0.5f);
        //Contenido Original de void Menu
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
        GO_DeslizamientoZoom.SetActive(false);
    }

    public void B_FCualquieraElegirSala_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }

    public void B_Gastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuBotonesMenuPrincipal.SetActive(true);
        GO_FGastronomia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Postgrado_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuBotonesMenuPrincipal.SetActive(true);
        GO_FPostgrado_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_Tecnologia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuBotonesMenuPrincipal.SetActive(true);
        //GO_FTecnologia_SgtEscena.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_CienciasdelaSalud_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuBotonesMenuPrincipal.SetActive(true);
        GO_DeslizamientoZoom.SetActive(true);
    }
    public void B_FGastronomia_Salir()
    {
        DesactivarElementosDePantalla();
        GO_MenuElementos.SetActive(true);
    }

    public void Menu_ModuloCienciasdelaSalud()
    {
        StartCoroutine(II_Menu_ModuloCienciasdelaSalud());
    }

    public IEnumerator II_Menu_ModuloCienciasdelaSalud()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_ModuloCienciasdelaSalud.SetActive(true);
    }

    public void B_Gastronomia()
    {
        StartCoroutine(II_B_Gastronomia());
    }

    public IEnumerator II_B_Gastronomia()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_FGastronomia.SetActive(true);
    }
    public void B_Postgrado()
    {
        StartCoroutine(II_B_Postgrado());
    }

    public IEnumerator II_B_Postgrado()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_FPostgrado.SetActive(true);
    }
    public void B_Tecnologia()
    {
        StartCoroutine(II_B_Tecnologia());
    }

    public IEnumerator II_B_Tecnologia()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_FTecnologia.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloA()
    {
        StartCoroutine(II_B_CienciasdelaSalud_ModuloA());
    }

    public IEnumerator II_B_CienciasdelaSalud_ModuloA()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloA.SetActive(true);
    }
    public void B_CienciasdelaSalud_ModuloC()
    {
        StartCoroutine(II_B_CienciasdelaSalud_ModuloC());
    }

    public IEnumerator II_B_CienciasdelaSalud_ModuloC()
    {
        yield return new WaitForSeconds(0.5f);
        DesactivarElementosDePantalla();
        GO_FCienciasDeLaSalud_ModuloC.SetActive(true);
    }

    public void LoadNextLevel_PantallaPrincipal()
    {
        StartCoroutine(levelLoader_LoadNextLevel_PantallaPrincipal());
    }

    public IEnumerator levelLoader_LoadNextLevel_PantallaPrincipal()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("PantallaPrincipal", LoadSceneMode.Single);
    }
    public void SalirMenuPantallaPrincipal()
    {
        LoadNextLevel_PantallaPrincipal();
    }

    public void RotarCambiarCamara()
    {
        xCambiarCamara = GO_CamaraRotacionMain.transform.rotation.eulerAngles.y;
        xCambiarCamara += 90;
        GO_CamaraRotacionMain.transform.rotation = Quaternion.Euler(GO_CamaraRotacionMain.transform.rotation.eulerAngles.x, xCambiarCamara, GO_CamaraRotacionMain.transform.rotation.eulerAngles.z);
    }

    public void B_FPostgrado_SgtEscena()
    {
        LoadNextLevel_ModuloPostgrado();
    }
    public void LoadNextLevel_ModuloPostgrado()
    {
        StartCoroutine(levelLoader_ModuloPostgrado());
    }
    public IEnumerator levelLoader_ModuloPostgrado()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Postgrado", LoadSceneMode.Single);
    }

    public void B_FGastronomia_SgtEscena()
    {
        LoadNextLevel_ModuloGastronomia();
    }
    public void LoadNextLevel_ModuloGastronomia()
    {
        StartCoroutine(levelLoader_ModuloGastronomia());
    }

    public IEnumerator levelLoader_ModuloGastronomia()
    {
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene("Modulo_Gastronomia", LoadSceneMode.Single);
    }

    //------------------------------------------Plataforma-----------------------------------------------------------
    public void B_Postgrado_PB_Entrada()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Postgrado = 102;
        PlayerPrefs.SetInt(F_Destino_Postgrado, ubicacion_Destino_Postgrado);
        //
    }
    //
    //------------------------------------------Gastronomia-----------------------------------------------------------
    public void B_Gastronomia_NumeroAula_G001()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 1;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G002()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 2;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G003()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 3;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G004()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 4;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G005()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 5;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G006()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 6;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G007()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 7;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G008()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 8;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G009()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 9;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G0010()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 10;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G0011()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 11;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G0012()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 12;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }

    //**1P
    public void B_Gastronomia_NumeroAula_G101()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 13;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G102()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 14;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G103()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 15;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G104()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 16;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G105()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 17;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G106()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 18;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G107()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 19;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_G108()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 20;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab1()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 21;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab2()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 22;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    public void B_Gastronomia_NumeroAula_Lab3()
    {
        //--- Guardar datos de Destino -- Gastronomia
        ubicacion_Destino_Gastronomia = 23;
        PlayerPrefs.SetInt(F_Destino_Gastronomia, ubicacion_Destino_Gastronomia);
        //
    }
    //
}
