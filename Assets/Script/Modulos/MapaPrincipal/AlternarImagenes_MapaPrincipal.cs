using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternarImagenes_MapaPrincipal : MonoBehaviour
{
    public Animator image;
    public Animator image1;

    public GameObject GO_image;
    public GameObject GO_image1;

    public void desactivarImages()
    {
        GO_image.SetActive(false);
        GO_image1.SetActive(false);
    }
    void Start()
    {
        StartCoroutine(AnimacionCarrusel());
    }

    public IEnumerator AnimacionCarrusel()
    {
        for (int i = 0; i < 100; i++)
        {
            GO_image.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image1.SetActive(false);
            image.SetTrigger("End");

            GO_image1.SetActive(true);
            yield return new WaitForSeconds(8);
            GO_image.SetActive(false);
            image1.SetTrigger("End");

            //desactivarImages();
        }
    }
}
