using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Postgrado_Destino_Scripttt : MonoBehaviour
{
    //**3P
    public GameObject Flechas_3P_Escaleras_PB;
    public GameObject Flechas_3P_Escaleras_1P;
    public GameObject Flechas_3P_Escaleras_2P;
    
    public GameObject Flechas_Aula_PG301;
    public GameObject Flechas_Aula_PG302;
    //**4P
    public GameObject Flechas_4P_Escaleras_PB;
    public GameObject Flechas_4P_Escaleras_1P;
    public GameObject Flechas_4P_Escaleras_2P;
    public GameObject Flechas_4P_Escaleras_3P;
    
    public GameObject Flechas_Aula_PG401;
    public GameObject Flechas_Aula_PG402;
    public GameObject Flechas_Aula_PG403;
    public GameObject Flechas_Aula_PG404;
    public GameObject Flechas_Aula_PG405;
    //**5P
    public GameObject Flechas_5P_Escaleras_PB;
    public GameObject Flechas_5P_Escaleras_1P;
    public GameObject Flechas_5P_Escaleras_2P;
    public GameObject Flechas_5P_Escaleras_3P;
    public GameObject Flechas_5P_Escaleras_4P;
    
    public GameObject Flechas_Aula_PG501;
    public GameObject Flechas_Aula_PG502;
    public GameObject Flechas_Aula_PG503;
    public GameObject Flechas_Aula_PG504;
    public GameObject Flechas_Aula_PG505;
    //**6P
    public GameObject Flechas_6P_Escaleras_PB;
    public GameObject Flechas_6P_Escaleras_1P;
    public GameObject Flechas_6P_Escaleras_2P;
    public GameObject Flechas_6P_Escaleras_3P;
    public GameObject Flechas_6P_Escaleras_4P;
    public GameObject Flechas_6P_Escaleras_5P;
    
    public GameObject Flechas_Aula_PG601;
    public GameObject Flechas_Aula_PG602;
    public GameObject Flechas_Aula_PG603;
    public GameObject Flechas_Aula_PG604;
    public GameObject Flechas_Aula_PG605;
    //**7P
    public GameObject Flechas_7P_Escaleras_PB;
    public GameObject Flechas_7P_Escaleras_1P;
    public GameObject Flechas_7P_Escaleras_2P;
    public GameObject Flechas_7P_Escaleras_3P;
    public GameObject Flechas_7P_Escaleras_4P;
    public GameObject Flechas_7P_Escaleras_5P;
    public GameObject Flechas_7P_Escaleras_6P;
    
    public GameObject Flechas_Aula_PG703;
    public GameObject Flechas_Aula_PG704;

    public GameObject Localizacion_PB_LD;
    public GameObject Localizacion_PB_MD;
    public GameObject Localizacion_PB_IZ;

    //--- Guardar datos de Destino -- Facultad
    public string F_Destino = "Postgrado";
    public int ubicacion_Destino;
    //
    public void Awake()
    {
        StartCoroutine(tempo());
    }
    public void Desactivador_Flechas_Aulas()
    {
        //PB_Localizacion
        Localizacion_PB_LD.SetActive(false);
        Localizacion_PB_MD.SetActive(false);
        Localizacion_PB_IZ.SetActive(false);
        //*3P
        Flechas_3P_Escaleras_PB.SetActive(false);
        Flechas_3P_Escaleras_1P.SetActive(false);
        Flechas_3P_Escaleras_2P.SetActive(false);
        
        Flechas_Aula_PG301.SetActive(false);
        Flechas_Aula_PG302.SetActive(false);
        //*4P
        Flechas_4P_Escaleras_PB.SetActive(false);
        Flechas_4P_Escaleras_1P.SetActive(false);
        Flechas_4P_Escaleras_2P.SetActive(false);
        Flechas_4P_Escaleras_3P.SetActive(false);
        
        Flechas_Aula_PG401.SetActive(false);
        Flechas_Aula_PG402.SetActive(false);
        Flechas_Aula_PG403.SetActive(false);
        Flechas_Aula_PG404.SetActive(false);
        Flechas_Aula_PG405.SetActive(false);
        //*5P
        Flechas_5P_Escaleras_PB.SetActive(false);
        Flechas_5P_Escaleras_1P.SetActive(false);
        Flechas_5P_Escaleras_2P.SetActive(false);
        Flechas_5P_Escaleras_3P.SetActive(false);
        Flechas_5P_Escaleras_4P.SetActive(false);
        
        Flechas_Aula_PG501.SetActive(false);
        Flechas_Aula_PG502.SetActive(false);
        Flechas_Aula_PG503.SetActive(false);
        Flechas_Aula_PG504.SetActive(false);
        Flechas_Aula_PG505.SetActive(false);
        //*6P
        Flechas_6P_Escaleras_PB.SetActive(false);
        Flechas_6P_Escaleras_1P.SetActive(false);
        Flechas_6P_Escaleras_2P.SetActive(false);
        Flechas_6P_Escaleras_3P.SetActive(false);
        Flechas_6P_Escaleras_4P.SetActive(false);
        Flechas_6P_Escaleras_5P.SetActive(false);
        
        Flechas_Aula_PG601.SetActive(false);
        Flechas_Aula_PG602.SetActive(false);
        Flechas_Aula_PG603.SetActive(false);
        Flechas_Aula_PG604.SetActive(false);
        Flechas_Aula_PG605.SetActive(false);
        //*7P
        Flechas_7P_Escaleras_PB.SetActive(false);
        Flechas_7P_Escaleras_1P.SetActive(false);
        Flechas_7P_Escaleras_2P.SetActive(false);
        Flechas_7P_Escaleras_3P.SetActive(false);
        Flechas_7P_Escaleras_4P.SetActive(false);
        Flechas_7P_Escaleras_5P.SetActive(false);
        Flechas_7P_Escaleras_6P.SetActive(false);
        
        Flechas_Aula_PG703.SetActive(false);
        Flechas_Aula_PG704.SetActive(false);
    }

    public IEnumerator tempo()
    {
        ubicacion_Destino = PlayerPrefs.GetInt(F_Destino, -1);
        if (ubicacion_Destino == 1)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_3P_Escaleras_PB.SetActive(true);
            Flechas_3P_Escaleras_1P.SetActive(true);
            Flechas_3P_Escaleras_2P.SetActive(true);
            
            Flechas_Aula_PG301.SetActive(true);
        }
        else if (ubicacion_Destino == 2)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_3P_Escaleras_PB.SetActive(true);
            Flechas_3P_Escaleras_1P.SetActive(true);
            Flechas_3P_Escaleras_2P.SetActive(true);
            
            Flechas_Aula_PG302.SetActive(true);
        }
        else if (ubicacion_Destino == 3)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_4P_Escaleras_PB.SetActive(true);
            Flechas_4P_Escaleras_1P.SetActive(true);
            Flechas_4P_Escaleras_2P.SetActive(true);
            Flechas_4P_Escaleras_3P.SetActive(true);
            
            Flechas_Aula_PG401.SetActive(true);
        }
        else if (ubicacion_Destino == 4)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_4P_Escaleras_PB.SetActive(true);
            Flechas_4P_Escaleras_1P.SetActive(true);
            Flechas_4P_Escaleras_2P.SetActive(true);
            Flechas_4P_Escaleras_3P.SetActive(true);
            
            Flechas_Aula_PG402.SetActive(true);
        }
        else if (ubicacion_Destino == 5)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_4P_Escaleras_PB.SetActive(true);
            Flechas_4P_Escaleras_1P.SetActive(true);
            Flechas_4P_Escaleras_2P.SetActive(true);
            Flechas_4P_Escaleras_3P.SetActive(true);
            
            Flechas_Aula_PG403.SetActive(true);
        }
        else if (ubicacion_Destino == 6)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_4P_Escaleras_PB.SetActive(true);
            Flechas_4P_Escaleras_1P.SetActive(true);
            Flechas_4P_Escaleras_2P.SetActive(true);
            Flechas_4P_Escaleras_3P.SetActive(true);
            
            Flechas_Aula_PG404.SetActive(true);
        }
        else if (ubicacion_Destino == 7)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_4P_Escaleras_PB.SetActive(true);
            Flechas_4P_Escaleras_1P.SetActive(true);
            Flechas_4P_Escaleras_2P.SetActive(true);
            Flechas_4P_Escaleras_3P.SetActive(true);
            
            Flechas_Aula_PG405.SetActive(true);
        }
        else if (ubicacion_Destino == 8)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_5P_Escaleras_PB.SetActive(true);
            Flechas_5P_Escaleras_1P.SetActive(true);
            Flechas_5P_Escaleras_2P.SetActive(true);
            Flechas_5P_Escaleras_3P.SetActive(true);
            Flechas_5P_Escaleras_4P.SetActive(true);
            
            Flechas_Aula_PG501.SetActive(true);
        }
        else if (ubicacion_Destino == 9)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_5P_Escaleras_PB.SetActive(true);
            Flechas_5P_Escaleras_1P.SetActive(true);
            Flechas_5P_Escaleras_2P.SetActive(true);
            Flechas_5P_Escaleras_3P.SetActive(true);
            Flechas_5P_Escaleras_4P.SetActive(true);
            
            Flechas_Aula_PG502.SetActive(true);
        }
        else if (ubicacion_Destino == 10)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_5P_Escaleras_PB.SetActive(true);
            Flechas_5P_Escaleras_1P.SetActive(true);
            Flechas_5P_Escaleras_2P.SetActive(true);
            Flechas_5P_Escaleras_3P.SetActive(true);
            Flechas_5P_Escaleras_4P.SetActive(true);
            
            Flechas_Aula_PG503.SetActive(true);
        }
        else if (ubicacion_Destino == 11)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_5P_Escaleras_PB.SetActive(true);
            Flechas_5P_Escaleras_1P.SetActive(true);
            Flechas_5P_Escaleras_2P.SetActive(true);
            Flechas_5P_Escaleras_3P.SetActive(true);
            Flechas_5P_Escaleras_4P.SetActive(true);
            
            Flechas_Aula_PG504.SetActive(true);
        }
        else if (ubicacion_Destino == 12)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_5P_Escaleras_PB.SetActive(true);
            Flechas_5P_Escaleras_1P.SetActive(true);
            Flechas_5P_Escaleras_2P.SetActive(true);
            Flechas_5P_Escaleras_3P.SetActive(true);
            Flechas_5P_Escaleras_4P.SetActive(true);
            
            Flechas_Aula_PG505.SetActive(true);
        }
        else if (ubicacion_Destino == 13)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_6P_Escaleras_PB.SetActive(true);
            Flechas_6P_Escaleras_1P.SetActive(true);
            Flechas_6P_Escaleras_2P.SetActive(true);
            Flechas_6P_Escaleras_3P.SetActive(true);
            Flechas_6P_Escaleras_4P.SetActive(true);
            Flechas_6P_Escaleras_5P.SetActive(true);
            
            Flechas_Aula_PG601.SetActive(true);
        }
        else if (ubicacion_Destino == 14)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_6P_Escaleras_PB.SetActive(true);
            Flechas_6P_Escaleras_1P.SetActive(true);
            Flechas_6P_Escaleras_2P.SetActive(true);
            Flechas_6P_Escaleras_3P.SetActive(true);
            Flechas_6P_Escaleras_4P.SetActive(true);
            Flechas_6P_Escaleras_5P.SetActive(true);
            
            Flechas_Aula_PG602.SetActive(true);
        }
        else if (ubicacion_Destino == 15)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_6P_Escaleras_PB.SetActive(true);
            Flechas_6P_Escaleras_1P.SetActive(true);
            Flechas_6P_Escaleras_2P.SetActive(true);
            Flechas_6P_Escaleras_3P.SetActive(true);
            Flechas_6P_Escaleras_4P.SetActive(true);
            Flechas_6P_Escaleras_5P.SetActive(true);
            
            Flechas_Aula_PG603.SetActive(true);
        }
        else if (ubicacion_Destino == 16)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_6P_Escaleras_PB.SetActive(true);
            Flechas_6P_Escaleras_1P.SetActive(true);
            Flechas_6P_Escaleras_2P.SetActive(true);
            Flechas_6P_Escaleras_3P.SetActive(true);
            Flechas_6P_Escaleras_4P.SetActive(true);
            Flechas_6P_Escaleras_5P.SetActive(true);
            
            Flechas_Aula_PG604.SetActive(true);
        }
        else if (ubicacion_Destino == 17)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_6P_Escaleras_PB.SetActive(true);
            Flechas_6P_Escaleras_1P.SetActive(true);
            Flechas_6P_Escaleras_2P.SetActive(true);
            Flechas_6P_Escaleras_3P.SetActive(true);
            Flechas_6P_Escaleras_4P.SetActive(true);
            Flechas_6P_Escaleras_5P.SetActive(true);
            
            Flechas_Aula_PG605.SetActive(true);
        }
        else if (ubicacion_Destino == 18)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_7P_Escaleras_PB.SetActive(true);
            Flechas_7P_Escaleras_1P.SetActive(true);
            Flechas_7P_Escaleras_2P.SetActive(true);
            Flechas_7P_Escaleras_3P.SetActive(true);
            Flechas_7P_Escaleras_4P.SetActive(true);
            Flechas_7P_Escaleras_5P.SetActive(true);
            Flechas_7P_Escaleras_6P.SetActive(true);
            
            Flechas_Aula_PG703.SetActive(true);
        }
        else if (ubicacion_Destino == 19)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Flechas_7P_Escaleras_PB.SetActive(true);
            Flechas_7P_Escaleras_1P.SetActive(true);
            Flechas_7P_Escaleras_2P.SetActive(true);
            Flechas_7P_Escaleras_3P.SetActive(true);
            Flechas_7P_Escaleras_4P.SetActive(true);
            Flechas_7P_Escaleras_5P.SetActive(true);
            Flechas_7P_Escaleras_6P.SetActive(true);
            
            Flechas_Aula_PG704.SetActive(true);
        }
        else if (ubicacion_Destino == 101)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Localizacion_PB_LD.SetActive(true);
        }
        else if (ubicacion_Destino == 102)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Localizacion_PB_MD.SetActive(true);
        }
        else if (ubicacion_Destino == 103)
        {
            yield return new WaitForSeconds(1);
            Desactivador_Flechas_Aulas();
            Localizacion_PB_IZ.SetActive(true);
        }
    }
}
